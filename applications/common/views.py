from django.views.generic.base import TemplateView
from django.views import View
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib.auth.views import logout
from datetime import datetime, timedelta
from applications.student.models import Student
from .models import Exam, TimeTable, Subject, Chapter, Question, Answer
from applications.result.models import Results, ExamResult
from django.http import JsonResponse
from datetime import datetime, timezone
from django.http import HttpResponseRedirect
from django.db.models import Q
import pytz



class DashboardView(View):

    @method_decorator(login_required)
    def get(self, request):
        obj = Student.objects.get(user=request.user)
        d0 = datetime.now().date()
        d1 = obj.exam_date.date
        delta = d1 - d0
        result_list = Results.objects.filter(student=request.user.student)
        daily_list = Results.objects.filter(first_time=True,exam__daily_paper=True,student=request.user.student).order_by('id')
        total_list = []
        attended_list = [round((obj.get_attended()/len(obj.exam.questions.all()))*100,2) for obj in daily_list]
        correct_list = [round((obj.get_correct()/len(obj.exam.questions.all()))*100,2) for obj in daily_list]
        date_list = daily_list

        return render(request, "dashboard.html", {'page': 'home', "days": delta.days,
                                                  'results':result_list,
                                                  'total_list':total_list,
                                                  'attended_list':attended_list,
                                                  'correct_list':correct_list,
                                                  'date_list':date_list
                                                  })


class ProfileView(View):

    @method_decorator(login_required)
    def get(self, request):
        return render(request, "profile.html", {'page': 'profile'})


class TimeTableView(View):

    @method_decorator(login_required)
    def get(self, request):
        time_table = request.user.student.batch.time_table.all()
        return render(request, "table.html", {'page': 'table', 'table': time_table})


class DailyView(View):

    @method_decorator(login_required)
    def get(self, request):
        batch = request.user.student.batch
        today = datetime.today()
        exams = Exam.objects.filter(batch=batch, daily_paper=True,
                                    hidden=False,
                                    exam_date__lte=today,
                                    )
        attended_exam = [obj.exam.id for obj in Results.objects.filter(student=request.user.student)]
        exams = exams.filter(~Q(id__in=attended_exam))
        return render(request, "daily.html", {'page': 'daily', "exams": exams})


class PracticeView(View):

    @method_decorator(login_required)
    def get(self, request):
        subjects = Subject.objects.all()
        return render(request, "practice.html", {'page': 'practice', 'subjects': subjects})


class ResultsView(View):

    @method_decorator(login_required)
    def get(self, request):
        result_list = Results.objects.filter(student=request.user.student)
        return render(request, "result.html", {'page': 'result','results':result_list})


class PaperView(View):

    @method_decorator(login_required)
    def get(self, request):
        hub = request.GET.get('hub',None)
        result = Results.objects.get(id=hub)
        return render(request, "final.html", {'page': 'final','result':result,'exam':result.exam})


from django.utils import timezone
import time

class ExamView(View):
    @method_decorator(login_required)
    def get(self, request, slug):

        exam = get_object_or_404(Exam, slug_field=slug)

        if request.GET.get('view', None):
            return render(request, "exam.html",
                          {'page': 'exam',
                           'questions': exam.questions.all(),
                           'exam': exam, 'result': "",
                           'endtime': ""
                           })


        try:
            result = Results.objects.get(id = request.session['result_id'])
        except:
            result = Results(exam=exam,
                             student=request.user.student,
                             start_time=datetime.now(tz=timezone.utc),
                             )
            if result.exam.time and not request.GET.get('practice',False):
                result.end_time = datetime.now(tz=timezone.utc) + timedelta(minutes=exam.time)

        temp_result = Results.objects.filter(exam=exam, student=request.user.student)
        if not temp_result:
            result.first_time = True
        result.save()
        
        if request.GET.get('end', 0):
            result.ended = True
            del request.session['result_id']
            del request.session['active_exam']
            del request.session['active_slug']
            result.save()
            return HttpResponseRedirect("/results/")

        if result.end_time:
            if result.end_time <= datetime.now(tz=timezone.utc) or request.GET.get('end', 0):
                result.ended = True
                del request.session['result_id']
                del request.session['active_exam']
                del request.session['active_slug']
                result.save()
                return HttpResponseRedirect("/results/")

        request.session['result_id'] = result.id
        request.session['active_exam'] = True
        request.session['active_slug'] = result.exam.slug_field

        date_int=None
        if result.end_time:
            date_int = int(time.mktime(result.end_time.timetuple()) * 1000)

        return render(request, "exam.html",
                      {'page': 'exam',
                       'questions': exam.questions.all(),
                        'exam': exam,'result':result,
                        'endtime':result.end_time,
                        'int_time':date_int
                       })

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple


class ExamForm(forms.ModelForm):


    class Meta:
        model=Exam
        exclude = ("question_m2m","slug_field")



class SubjectListView(View):

    @method_decorator(login_required)
    def get(self, request, **kwargs):
        id = kwargs['id']
        previous_list = []
        if int(id) == 0:
            today = datetime.today()
            previous_list = Exam.objects.filter(daily_paper=True,hidden=False, exam_date__lt=today)
            return render(request, "list.html", {'page': 'list', 'previous_list': previous_list})

        if 'paper' in kwargs:
            paper = kwargs['paper']
        else:
            paper = []
        obj = get_object_or_404(Subject, id=id)
        try:
            papers = Exam.objects.filter(chapter=paper, daily_paper=False,hidden=False)
        except:
            papers = []

        return render(request, "list.html", {'page': 'list', 'chapters': obj.chapter.all(),
                                             "papers": papers,
                                             'chapter_id': int(paper) if paper else None,
                                             'sub': obj.id,
                                             'previous_list': previous_list,
                                             })


# class LoginView(TemplateView):
#     template_name = 'login.html'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super(LoginView, self).get_context_data(*args, **kwargs)
#         context['page'] = 'login'
#         return context

class LoginView(View):

    def get(self, request):
        return render(request, "login.html", {})

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user and user.is_active:
            login(request, user)
            return redirect('/')
        else:
            return render(request, "login.html", {"error": "Invalid credentials", 'username': username})


class LogoutView(View):

    @method_decorator(login_required)
    def get(self, request):
        logout(request)
        return redirect('/login/')


class ExamAttendView(View):
    @method_decorator(login_required)
    def get(self, request):
        try:
            result = Results.objects.get(id=request.session['result_id'])
            q_id = Question.objects.get(id=request.GET.get('q_id'))
            ans_id = Answer.objects.get(id=request.GET.get('a_id'))
            student = request.user.student
            try:
                exam_q_obj = ExamResult.objects.get(result=result, question=q_id, student=student)
            except:
                exam_q_obj = ExamResult(result=result, question=q_id, student=student)

            if(exam_q_obj.answer == ans_id):
                exam_q_obj.delete()
                return JsonResponse({'success': 1, "error": "",'delete':1})
            exam_q_obj.answer = ans_id
            exam_q_obj.save()
            return JsonResponse({'success': 1,"error":"",'delete':0})
        except Exception as e:
            return JsonResponse({'success': 0, "error": str(e),'delete':0})


class ExamCreateView(View):
    @method_decorator(login_required)
    def get(self, request,id):
        exam = Exam.objects.get(id=int(id))
        subject = Subject.objects.all()
        questions = Question.objects.all()
        chapter_tot =[]
        if int(request.GET.get('subject',0)):
            sub = Subject.objects.get(id=request.GET.get('subject',None))
            chapter_tot = Chapter.objects.filter(subject=sub)
            questions = questions.filter(chapter__subject_id=request.GET.get('subject',0))
        if int(request.GET.get('chapter',0)):
            chpt = Chapter.objects.get(id=request.GET.get('chapter', 0))
            questions = questions.filter(chapter=chpt)
        list_relation = request.GET.get('list', 'empty')
        if list_relation != 'empty':
            exam.questions.clear()
            if list_relation :
                m2m_obj = Question.objects.filter(pk__in = list_relation.split(","))
                if m2m_obj:
                    exam.questions.add(*list(m2m_obj))
                    exam.save()

        return render(request, "exam_temp.html",{'q':questions,'exam_q':exam.questions.all(),
                                                 'exam':exam,'subject':subject,'chapter':chapter_tot,
                                                 'subject_id':int(request.GET.get('subject',0)),
                                                 'chapter_id':int(request.GET.get('chapter',0))})



class ExamListView(View):
    @method_decorator(login_required)
    def get(self, request):

        exam = Exam.objects.all()
        return render(request, "exam_list.html",{'exam':exam,'page':'manage'})


from django.core.mail import send_mail
from django.template.loader import render_to_string

class AnalyticsView(View):
    @method_decorator(login_required)
    def get(self, request):
        filter = request.GET.get('filter',0)
        student = request.GET.get('student', None)
        students = Student.objects.all()
        if student == "None" or student == None:
            return render(request, "analytics.html", {'page': 'analytics',
                                                      'total_list': [],
                                                      'attended_list': [],
                                                      'correct_list': [],
                                                      'date_list': [],
                                                      'students': students,
                                                      })


        daily_list = Results.objects.filter(first_time=True, exam__daily_paper=True,
                                            student__id=student).order_by('id')



        if filter and filter != '0':
            days = datetime.today() - timedelta(days=int(filter))
            daily_list = daily_list.filter(start_time__gte=days)
            filter = int(filter)
        total_list = []
        attended_list = [round((obj.get_attended() / len(obj.exam.questions.all())) * 100, 2) for obj in daily_list]
        correct_list = [round((obj.get_correct() / len(obj.exam.questions.all())) * 100, 2) for obj in daily_list]
        date_list = daily_list
        if student:
            student_obj = Student.objects.get(id=student)
            student = int(student)
        else:
            None
        msg = ""
        if int(request.GET.get('mail', 0)) and student and student_obj.parent_email:
            msg_html = render_to_string('mail_list.html', {'daily_table': date_list})
            msg = "Mail sent successfully"
            try:
                send_mail('Results: '+student_obj.user.get_full_name(), 'Elip Exam Results', 'admin@elip.com', [student_obj.parent_email],
                          html_message=msg_html,fail_silently=False)
            except Exception as e:
                msg = str(e)


        return render(request, "analytics.html", {'page': 'analytics',
                                                  'total_list': total_list,
                                                  'attended_list': attended_list,
                                                  'correct_list': correct_list,
                                                  'date_list': date_list,
                                                  'students':students,
                                                  'current_student':student,
                                                  'filter':filter,
                                                  'daily_table':date_list,
                                                  'student_obj':student_obj,
                                                  'msg':msg

                                                  })


class PreviewView(View):
    @method_decorator(login_required)
    def get(self, request,id):
        exam = Exam.objects.get(id=id)
        return render(request, "preview.html",{'exam':exam})