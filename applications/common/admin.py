from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Batch, ExamDate, Subject, Chapter, TimeTable, TimeTableTime,Question, Answer, Exam


class BatchAdmin(admin.ModelAdmin):
    list_display = ['name', 'start', 'end', ]

class ExamAdmin(admin.ModelAdmin):

    list_display = ['exam_name', 'date', ]

class ChapterInline(admin.StackedInline):
        model = Chapter
        extra = 1

class SubjectAdmin(admin.ModelAdmin):
    list_display = ['name', ]
    inlines = [ChapterInline, ]



class TimeInline(admin.StackedInline):
        model = TimeTableTime
        extra = 1


class TimeTableAdmin(admin.ModelAdmin):
    list_display = ['batch', 'date', ]
    inlines = [TimeInline, ]

class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 1

class QuestionAdmin(admin.ModelAdmin):
    search_fields = ('question', )
    list_filter = ('chapter__subject','chapter')
    inlines = [AnswerInline, ]

from django.contrib.admin import widgets
class ExamViewAdmin(admin.ModelAdmin):
    filter_vertical = ('questions',)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        vertical = False  # change to True if you prefer boxes to be stacked vertically
        kwargs['widget'] = widgets.FilteredSelectMultiple(
            db_field.verbose_name,
            vertical,
        )
        return super(ExamViewAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


admin.site.register(Batch, BatchAdmin)
admin.site.register(ExamDate, ExamAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(TimeTable, TimeTableAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Exam, ExamViewAdmin)
