import re
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.safestring import mark_safe
from django.template.defaultfilters import slugify
from django.apps import apps



class Batch(models.Model):
    start = models.DateField(_("Batch Start Date"))
    name = models.CharField(_("Batch Name"), max_length=200)
    end = models.DateField(_("Batch End Date"), null=True, blank=True)

    def __str__(self):
        return self.name + "(" + str(self.start) + " to " + str(self.end) + ")"

    class Meta:
        verbose_name = _('Batch')
        verbose_name_plural = _('Batch')


class ExamDate(models.Model):
    date = models.DateField(_("Exam Date"))
    exam_name = models.CharField(_("Exam Name"), max_length=200)

    def __str__(self):
        return self.exam_name + str(self.date)

    class Meta:
        verbose_name = _('Exam Date')
        verbose_name_plural = _('Exams Date')


class Subject(models.Model):
    name = models.CharField(_("Subject Name"), max_length=200)
    image = models.ImageField(_("Subject Image"), upload_to='pic_folder/', null=True, blank=True)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.name

    def slug(self):
        return slugify(self.title)

    def papers(self):
        return Exam.objects.filter(chapter__subject = self)

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


class Chapter(models.Model):
    subject = models.ForeignKey(Subject,related_name="chapter", null=True, blank=True)
    name = models.CharField(_("Chapter Name"), max_length=200)

    def __str__(self):
        return self.subject.name +" "+self.name


class TimeTable(models.Model):
    batch = models.ForeignKey(Batch, null=True, blank=True, related_name="time_table")
    date = models.DateField(_('Date'))


    def __str__(self):
        return str(self.date)

    class Meta:
        ordering = ('-date',)
        verbose_name = _('Time Table')
        verbose_name_plural = _('Time Table')


class TimeTableTime(models.Model):
    batch = models.ForeignKey(TimeTable,related_name="table_time")
    time_start = models.TimeField(_('Time Start'))
    time_end = models.TimeField(_('Time End'))
    subject = models.ForeignKey(Subject, null=True, blank=True)
    chapter = models.ForeignKey(Chapter, null=True, blank=True)


class Exam(models.Model):
    name = models.CharField(_('Exam Name'), max_length=150)
    batch = models.ForeignKey(Batch, null=True, blank=True)
    exam_date = models.DateField(_('Exam Date'), help_text="Please provide date in case of Daily test paper ",
                                 max_length=150, null=True, blank=True)
    chapter = models.ForeignKey(Chapter, null=True, blank=True, help_text="Select chapter in case of Practice exam ",related_name='papers',)
    time = models.IntegerField(_('Total Time'), null=True, blank=True,
                               help_text="Total time duration of exam in minuets")
    daily_paper = models.BooleanField(_('Daily Question Paper'), default=False,
                                      help_text="Check if this is daily question paper ")
    hidden = models.BooleanField(_('Hidden Paper ?'), default=False,
                                      help_text="Check if this paper is hidden ")
    slug_field = models.SlugField(max_length=100,null=True,blank=True)

    question_m2m = models.ManyToManyField('Question', blank=True,related_name='questions_all')

    def attended(self):
        return len(self.questions.all(attended=True))

    def __str__(self):
        return mark_safe(self.name)

    def exam_attended(self):
        return Results.objects.filter(student=requset.user.student).exist()

    def slug(self):
        return slugify(self.name)

    def get_first_count(self):
        return len(self.exam.filter(first_time =True))

    def save(self, *args, **kwargs):
        self.slug_field = slugify(self.name) + str(self.time)
        super(Exam, self).save(*args, **kwargs)


class Question(models.Model):
    exam = models.ManyToManyField('Exam', through=Exam.question_m2m.through, related_name="questions",blank=True)
    chapter = models.ManyToManyField(Chapter, null=True, blank=True, related_name="chapter_questions")
    question = RichTextUploadingField(_('Question'))
    q_explanation = models.TextField(_('Question Explanation'), default="No Explanation")
    a_explanation = RichTextUploadingField(_('Answer Explanation'), null=True, blank=True)


    def __str__(self):
        TAG_RE = re.compile(r'<[^>]+>')
        return TAG_RE.sub('', self.question.replace("&nbsp;", " ").replace("&acirc;",""))



class Answer(models.Model):
    question = models.ForeignKey(Question, related_name="answer")
    answer = RichTextUploadingField(_('Answer'))
    correct = models.BooleanField(_('Correct Answer'), default=False)

    def __str__(self):
        return mark_safe(self.answer)


