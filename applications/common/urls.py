"""elips URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from .views import DashboardView, ProfileView, TimeTableView, DailyView, PracticeView, ResultsView, PaperView, ExamView, \
    SubjectListView, LoginView, LogoutView, ExamAttendView, ExamCreateView, ExamListView, AnalyticsView, PreviewView

urlpatterns = [
    url(r'^profile/', ProfileView.as_view(), name='profile'),
    url(r'^time-table/', TimeTableView.as_view(), name='time-table'),
    url(r'^daily-paper/', DailyView.as_view(), name='daily-paper'),
    url(r'^practice/', PracticeView.as_view(), name='practice'),
    url(r'^results/', ResultsView.as_view(), name='results'),
    url(r'^final/', PaperView.as_view(), name='final'),
    url(r'^exam/(?P<slug>[\w-]+)/$', ExamView.as_view(), name='exam'),
    url(r'^subject/(?P<id>[-\w\d]+)/(?P<paper>[-\w\d]+)/', SubjectListView.as_view(), name='list'),
    url(r'^subject/(?P<id>[-\w\d]+)/', SubjectListView.as_view(), name='list'),
    url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name='logout'),
    url(r'^exam-q-attend/', ExamAttendView.as_view(), name='qs_attend_view'),
    url(r'^exam-create/(?P<id>[-\w\d]+)/', ExamCreateView.as_view(), name='exam_create_view'),
    url(r'^exam-list/', ExamListView.as_view(), name='exam_list_view'),
    url(r'^analytics/', AnalyticsView.as_view(), name='analytics_view'),
    url(r'^exam-preview/(?P<id>[-\w\d]+)/', PreviewView.as_view(), name='preview_view'),
    url(r'', DashboardView.as_view(), name='dashboard'),
]
