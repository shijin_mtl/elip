from applications.student.models import StudentData as UserModel


class MyAuthBackend(object):
    def authenticate(self, username, password):
        try:
            user = UserModel.objects.get(username=username,password=password)
            # if user.check_password(password):
            #     return user
            # else:
            #     return None
            if user:
                return user
            else:
                return None
        except UserModel.DoesNotExist:
            return None
        except Exception as e:
            logging.getLogger("error_logger").error(repr(e))
            return None

    def get_user(self, user_id):
        try:
            user = UserModel.objects.get(id=user_id)
            if user.is_active:
                return user
            return None
        except UserModel.DoesNotExist:
            return None
