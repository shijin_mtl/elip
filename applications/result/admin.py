from .models import  Results, ExamResult
from django.contrib import admin


class ResultAdmin(admin.ModelAdmin):
    list_filter = ('student','exam',)
    list_display = ('exam','student','start_time','get_question_count','get_attended' )




admin.site.register(Results, ResultAdmin)
admin.site.register(ExamResult, admin.ModelAdmin)