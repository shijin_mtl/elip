from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.safestring import mark_safe
from django.template.defaultfilters import slugify
from django.apps import apps
from applications.student.models import Student
from applications.common.models import Exam,Question,Answer




class Results(models.Model):
    exam = models.ForeignKey(Exam, related_name="exam")
    student = models.ForeignKey(Student,related_name="student_result")
    first_time = models.BooleanField(_('First Time Attending'), default=False)
    time_taken = models.CharField(_('Total Time Taken'), max_length=50, null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)
    ended = models.BooleanField(_('Ended'), default=False)

    def __str__(self):
        return self.exam.name+":"+self.student.user.first_name+ " "+ self.student.user.last_name

    def get_history(self):
        return [obj.answer.id for obj in self.result.all()]

    def get_correct(self):
        q_a_list = ExamResult.objects.filter(result=self)
        score = 0
        for obj in q_a_list:
            if obj.answer.correct:
                score += 1
        return score

    def get_score_level(self):
        if len(self.exam.questions.all()):
            return 100 * int(self.get_correct())/len(self.exam.questions.all())
        else:
            return 0

    def get_question_count(self):
        return  len(self.exam.questions.all())



    def get_attended_list(self):
        q_a_list = [obj.answer.id for obj in ExamResult.objects.filter(result=self)]
        return q_a_list

    def get_attended(self):
        q_a_list = ExamResult.objects.filter(result=self)
        return len(q_a_list)

    def q_attended(self):
        q_list = [obj.question.id for obj in ExamResult.objects.filter(result=self)]
        return q_list

    def q_verify(self,q):
        try:
            r = ExamResult.objects.get(result =self,question=q )
            if r.answer.correct:
                return 1
            else:
                return 0
        except:
            return "A"

    class Meta:
        ordering = ('-start_time',)
        verbose_name = 'Result'
        verbose_name_plural = 'Results'


class ExamResult(models.Model):
    result = models.ForeignKey(Results, related_name="result",null=True,blank=True)
    question = models.ForeignKey(Question, related_name="result_qus",null=True,blank=True)
    answer = models.ForeignKey(Answer, related_name="result_ans",null=True,blank=True)
    student = models.ForeignKey(Student, related_name="result_student",null=True,blank=True)

    def __str__(self):
        return self.result.exam.name



