from django.db import models
from django.utils.translation import ugettext_lazy as _
from applications.common.models import ExamDate, Subject
from django.contrib.auth.models import User



class FacultyModel(models.Model):
    faculty = models.OneToOneField(User, related_name="faculty", on_delete=models.CASCADE, null=True, blank=True)
    phone = models.CharField(_("Phone No."), max_length=20)
    subject = models.ForeignKey(Subject, null=True, blank=True)

    def __str__(self):
        return self.faculty.first_name +" "+self.faculty.last_name

    class Meta:
        verbose_name = _('Faculty')
        verbose_name_plural = _('Faculties')


