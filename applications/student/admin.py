from .models import Student
from django.contrib.auth.models import User
from django.contrib import admin



class InnerUserInline(admin.StackedInline):
    model = User

class StudentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Student, StudentAdmin)
