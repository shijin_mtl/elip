# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-24 15:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0004_auto_20181023_1841'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='results',
            name='exam',
        ),
        migrations.RemoveField(
            model_name='results',
            name='student',
        ),
        migrations.DeleteModel(
            name='Results',
        ),
    ]
