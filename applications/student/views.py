from django.views import View
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from applications.common.models import Exam
from .models import Student
from applications.result.models import Results





class ResultView(View):
    @method_decorator(login_required)
    def get(self, request):
        paper_id =request.GET.get("paper",None)
        if paper_id:
            exam = Exam.objects.get(id=paper_id)
            students = Student.objects.filter(batch=exam.batch)
            main_list = {}
            for student in students:
                mark_list = []
                try:
                    result = Results.objects.get(exam=exam, student=student, first_time=True)
                    for q in exam.questions.all():
                        mark_list.append(result.q_verify(q))
                    main_list[student.user.first_name] = {'mark_list':mark_list,
                                                          'ct_list':mark_list.count(1),
                                                          'wa_list': mark_list.count(0),
                                                          'na_list': mark_list.count('A'),
                                                          'tq_list': len(mark_list),
                                                          'tt_list':mark_list.count(1)+mark_list.count(0)
                                                          }



                except:
                    pass

            if request.GET.get('detail',None):
                return render(request, "result_detail.html", {'paper_id': paper_id,
                                                             'exam': exam, 'students': students, 'main_list':
                                                                 main_list})



            return render(request, "daily_result.html", {'paper_id': paper_id,
                                                         'exam':exam,'students':students,'main_list':
                                                             main_list})
        else:
            exams = Exam.objects.filter(daily_paper=True)
            return render(request,"daily_result.html",{'daily_paper':exams,'paper_id':paper_id})