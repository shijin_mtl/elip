from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from applications.common.models import ExamDate, Batch, Exam


class Student(models.Model):
    image = models.ImageField(upload_to='profile/', null=True, blank=True)
    user = models.OneToOneField(User, related_name="student", on_delete=models.CASCADE,null=True,blank=True)
    dob = models.DateField(_("DOB"), null=True, blank=True)
    phone = models.CharField(_("Phone No."), max_length=20, null=True, blank=True)
    reg_no = models.CharField(_("Register No."), max_length=20)
    parent_name = models.CharField(_("Parent Name"), max_length=20, null=True, blank=True)
    parent_email = models.CharField(_("Parent Email"), max_length=100, null=True, blank=True)
    parent_phone = models.CharField(_("Parent Phone"), max_length=20, null=True, blank=True)
    date_of_join = models.DateField(_("Date of join"), null=True, blank=True)
    exam_date = models.ForeignKey(ExamDate, null=True, blank=True)
    batch = models.ForeignKey(Batch, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

    def get_exam_count(self):
        return len(self.student_result.filter(exam__daily_paper=True, first_time=True))

    def attended_exams(self):
        attended_list = Results.objects.filter(student=self)
        list = []
        for obj in attended_list:
            list.append(obj.id)
        return list

    def exam_score(self):
        results = self.student_result.all()
        score = [round(obj.get_score_level(),2) for obj in results]
        date = [obj.start_time.strftime('%m/%d/%Y') for obj in results]
        return ({'score':score,'date':date})





    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')


